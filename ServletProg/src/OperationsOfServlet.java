

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class OperationsOfServlet
 */
@WebServlet("/OperationsOfServlet")
public class OperationsOfServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	String companyname;
	String age;
	
	
    public OperationsOfServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
	public void init(ServletConfig config) throws ServletException 
	{
		// TODO Auto-generated method stub
		 companyname = config.getInitParameter("mycompany");
		 System.out.println(companyname);
		 age = config.getInitParameter("myage");
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) 
        {
           
            /* TODO output your page here. You may use following sample code. */
            String uname = request.getParameter("email");
            String upass = request.getParameter("password");
            out.println("username :"+uname+" "+"Password :"+upass);
            out.println("\nCompany name is: " + companyname );
            out.println("<br>age is: " + age);
            out.println("<h1>Servlet RequestResponse of project " + request.getContextPath() + "</h1>");
        }
		doGet(request, response);
	}

}

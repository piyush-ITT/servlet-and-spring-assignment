/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author piyush.tiwari
 */
@WebServlet(name = "ServletOfLogin", urlPatterns = {"/ServletOfLogin"})
public class ServletOfLogin extends HttpServlet 
{

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>ServletOfLogin</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServletOfLogin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    // Method is checking that logging user is exists or not in database 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        String str;
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/FormStore", "root", "gameover"); 
            PreparedStatement ps = conn.prepareStatement("select *from userInfo where email='" 
                    + request.getParameter("username") + "' and password='" + request.getParameter("password") + "'");
            ResultSet rs = ps.executeQuery();
        if (rs.next())
        {
            request.getSession().setAttribute("userId", rs.getString(1));
            RequestDispatcher rd = request.getRequestDispatcher("Homepage"); 
            rd.forward(request, response);   // forwarding to Homepage servlet
        }
        else
        {
            RequestDispatcher rd = request.getRequestDispatcher("index.html");            
            PrintWriter out = response.getWriter();
            rd.include(request, response); // sending back to login page
            out.print("\n");
            out.print("<html>");
            out.print("<head>");
            out.print("<link rel=\"stylesheet\" href=\"fortable.css\" type=\"text/css\"/>");
            out.print("</head");
            out.print("<body>");
            out.println("<p class='forpara'>Invalid username or password</p>");
            out.print("</body>");
            out.print("</html>");
        }
       }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException | ServletException | IOException e) 
        {
            response.getWriter().println(e.getMessage());
        }
    }
      /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */   
     @Override
    public String getServletInfo() 
    {
        return "Short description";
    }// </editor-fold>
}

   
   


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author piyush.tiwari
 */
@WebServlet(name = "Homepage", urlPatterns = {"/Homepage"})
public class Homepage extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Homepage</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Homepage at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    //Method for displaying user information on servlet using database
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
        try 
        {
              /*checks for session value for validation*/
            if(request.getSession().getAttribute("userId")==null)
            {
                response.sendRedirect("login.html");
            }
            printInformation(request, response);
        } catch (SQLException ex) 
        {
            Logger.getLogger(Homepage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*providing information of all user stored in database in the table form */
	private void printInformation(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException 
	{
                        // TODO Auto-generated method stub
		        String str;
		        ResultSet resultSet = null; 
		        try 
		        {
		            Class.forName("com.mysql.jdbc.Driver").newInstance();
		            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/FormStore", "root", "gameover");
		            PreparedStatement ps = conn.prepareStatement("select *from userInfo");
			        resultSet = ps.executeQuery();
		        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) 
		        {
		            response.getWriter().println(e.getMessage());
		        }
		        PrintWriter out = response.getWriter();
		        out.print("\n");
		        out.print("<html>");
                        out.print("<head>");
                        out.print("<link rel=\"stylesheet\" href=\"fortable.css\" type=\"text/css\"/>");
                        out.print("</head");
		        out.print("<body>");
		        out.print("<table class='decoration'>");
                        out.print("<caption>Login Information</caption>");
		        out.print("<tr>");
                        out.print("<th>");
		        out.print("First Name");
		        out.print("</th>");
                        out.print("<th>");
		        out.print("Last Name");
		        out.print("</th>");
		        out.print("<th>");
		        out.print("password");
		        out.print("</th>");
		        out.print("<th>");
		        out.print("username");
		        out.print("</th>");
		        out.print("<th>");
		        out.print("Mobile Number");
		        out.print("</th>");
		        out.print("<th>");
		        out.print("Gender");
		        out.print("</th>");
		        out.print("</tr>");
		        while (resultSet.next()) 
		        {

		            out.print("<tr>");
		            for (int i = 1; i <= 6; i++)
		            {
                                  /*Display a spefic column form the datebase*/
		                    out.print("<td>" + resultSet.getString(i) + "</td>");
		            }
		            out.print("</tr>");
		        }
		        out.print("</table>");
		        out.print("<form class='forlogout' action='LogOut' method=\"post\">");
		        out.print("<input type=\"submit\" value=\"Log out\" name=\"logout\"/>");
		        out.print("</form>");
		        out.print("</body>");
		        out.print("</html>");
	}

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() 
    {
        return "Short description";
    }// </editor-fold>
}

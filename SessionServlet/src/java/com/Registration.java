/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author piyush.tiwari
 */
@WebServlet(name = "Registration", urlPatterns = {"/Registration"})
public class Registration extends HttpServlet
{
     int flag=1;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Registration</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Registration at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException 
    {
       // processRequest(request, response);
        try 
        {
            registerInformation(request, response);  // method for registering user
        } catch (IOException | SQLException e) 
        {
            response.getWriter().println(e.getMessage());
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() 
    {
        return "Short description";
    }// </editor-fold>
    /* Method definition for registering user into database */
    private void registerInformation(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ServletException 
    {
        String str;
        Connection conn = null;
        try
        {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/FormStore", "root", "gameover");
        }
        catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e)
        {
            response.getWriter().println(e.getMessage());
        }
        PreparedStatement ps1 = conn.prepareStatement("select *from userInfo where email='" + request.getParameter("username") + "'");
        ResultSet rs = ps1.executeQuery();
        if (rs.next()) 
        {
            response.getWriter().print("Sorry username already Exist");     // chaecking condition for user already exists or not
        }
        else
        {
            String FirstName=request.getParameter("first_name");
            String LastName=request.getParameter("last_name");
            String Password=request.getParameter("password");
            String UserName=request.getParameter("username");
            String Phone=request.getParameter("phone");
            String Gender=request.getParameter("gender");

            ForValidation(FirstName,LastName,Password,UserName,Phone,Gender,response);
             PrintWriter out = response.getWriter();
            
            if(flag==1)
            {
                str = "Insert into userInfo values(?,?,?,?,?,?)";
                PreparedStatement ps = conn.prepareStatement(str);
                ps.setString(1, FirstName);
                ps.setString(2, LastName);
                ps.setString(3, Password);
                ps.setString(4, UserName);
                ps.setString(5, Phone);
                ps.setString(6, Gender);
                ps.executeUpdate();
                response.getWriter().println("Registered successfully");
                request.getSession().setAttribute("userId", request.getParameter("username"));
                RequestDispatcher rd = request.getRequestDispatcher("Homepage");
                rd.forward(request, response);
            }
        }
    }
    // Validation method for checking password length and null values
    private void ForValidation(String FirstName, String LastName, String Password, String UserName, String Phone, String Gender, HttpServletResponse response) throws IOException 
    {
         PrintWriter out = response.getWriter(); 

           // String check = null;
            if (UserName.equals("") || FirstName.equals("") || LastName.equals("") || Gender.equals("") || Phone.equals("")) 
            {
                out.println("field must not be empty");
            } 
            else if (Password.length()<=6) 
            {
                out.println("<h2>Password field should have atleast 6 correct</h2>");
                flag=0;
            }
            else
            {
                flag=1;
            }
    }

}
           
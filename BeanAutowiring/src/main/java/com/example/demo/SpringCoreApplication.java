package com.example.demo;

//import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.example.demo.configuration.AppConfig;
import com.example.demo.domain.Application;

@SpringBootApplication
public class SpringCoreApplication 
{

	 private static AbstractApplicationContext context;

	public static void main(String args[])
	 {
	        context = new AnnotationConfigApplicationContext(AppConfig.class);
	        Application application = (Application)context.getBean("application");
	        System.out.println("Application Details : "+application);
	 }
}

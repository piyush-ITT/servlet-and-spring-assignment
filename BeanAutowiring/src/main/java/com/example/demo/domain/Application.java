package com.example.demo.domain;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("application")
public class Application 
{
   @Autowired
   private ApplicationUser user;

   @Override
   public String toString() 
   {
       return "Application [user=" + user + "]";
   }
}
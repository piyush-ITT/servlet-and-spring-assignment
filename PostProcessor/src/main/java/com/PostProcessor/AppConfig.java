package com.PostProcessor;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages="com.PostProcessor")
public class AppConfig 
{
	
} 
package com.PostProcessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class MyBeanPostProcessor implements BeanPostProcessor
{

	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("Inside post process after initialization: "+beanName);
		return bean;
	}

	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("Inside post process before initialization: "+beanName);
		return bean;
	}

}

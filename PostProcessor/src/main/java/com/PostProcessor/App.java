package com.PostProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {

	public static void main(String args[]) 
	{
		//for(int i=0;i<2;i++)
		//{
			AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		    ctx.register(AppConfig.class);
		   ctx.refresh();
		    ElephantService service = ctx.getBean(ElephantService.class);
		    System.out.println(service);
		    service.print();
	    	ctx.registerShutdownHook();
	    	
	    	ctx.close();
		//}
	}

}

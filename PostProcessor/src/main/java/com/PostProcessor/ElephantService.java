package com.PostProcessor;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@SuppressWarnings("restriction")
@Component
@Scope("prototype")
public class ElephantService {
	@PostConstruct
	public void init() {
		System.out.println("Inside init method");
	}	
	public void print() {
		System.out.println("Hello World!");
	}
	@PreDestroy
	public void destroy() {
		System.out.println("Inside destroy method");		
	}
} 


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <style type="text/css" rel="stylesheet">
                body
                {
                  background:#d2eff2;
                }
                form
                {
                  margin-left: 35%;
                  margin-top: 40px;
                  font-size: 15px;
                }
                .field
                {
                  width: 75%;
                  height: 20px;
                  font-size: 15px;
                  border-radius: 3px;
                }
                header
                {
                  text-align: center;
                  font-family: cursive;
                  color: blue;
                  margin-top: 20px;
                  font-weight: normal;
                  text-shadow: 2px 1px #2810A2;
                  font-size: 30px;
                }
                .forsize
                {
                  background-color:#1A44EF;
                  width: 60%;
                  height: 30px;
                  font-size: 15px;
                  text-decoration: none;
                }
                form .single
                {
                  font-size: 10px;
                  height: -20px;
                }
                .details
                {
                  border: 2px #6DF3FC groove;
                  height: 250px;
                  width: 320px;
                  padding: 20px 10px 0px 80px;
                  border-radius: 15px;
                }
                .creating a:hover
                {
                  color: red; 
                }
            </style>
<title>Login</title>
</head>
<body>
    <header>
        <h1>Login here</h1>
    </header>
        <form action="loginProcess" method="post" modelAttribute="login">  
            <div class="details">
                <label >username</label>
                <br>
                <input class="field" type="text" name="username" required />
                
                <br><br>
                <label>Password</label>
                <br>
                <input class="field" type="password" name="password" required /><br><br>
                <input class="single" type="checkbox" checked="checked" value="Remember me"/>Remember me <br>
                <input class="forsize" type="submit" value="login" />
                <div class="creating">
                    <p><a href="<c:url value="/register" />">not having an account? sign up here</a></p>
                </div>
                <table>
		<tr>
			<td style="font-style: italic; color: red;">${message}</td>
                        <td style="font-style: italic; color: red;">${logoutMessage}</td>
		</tr>
                </table>
            </div>     
        </form> 
</body>
</html>

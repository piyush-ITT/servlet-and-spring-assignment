<%-- 
    Document   : register
    Created on : Aug 10, 2017, 3:51:16 PM
    Author     : piyush.tiwari
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
            <style type="text/css" rel="stylesheet">
                body
                {
                  background:#d2eff2;
                }
                form
                {
                  margin-left: 35%;
                  margin-top: 40px;
                  font-size: 15px;
                }

                .field
                {
                  width: 75%;
                  height: 20px;
                  font-size: 15px;
                  border-radius: 3px;
                }
                header
                {
                  text-align: center;
                  font-family: cursive;
                  color: blue;
                  margin-top: 20px;
                  font-weight: normal;
                  text-shadow: 2px 1px #2810A2;
                  font-size: 30px;
                }
                .creating a:hover
                {
                  color: red; 
                }
                .forsize
                {
                  background-color:#1A44EF;
                  width: 60%;
                  height: 30px;
                  font-size: 15px;
                  text-decoration: none;
                }
                .regdetails
                {
                  border: 2px #6DF3FC groove;
                  height: 500px;
                  width: 35%;
                  padding: 20px 10px 0px 80px;
                  border-radius: 15px;
                }
                form .single
                {
                  font-size: 10px;
                  height: -20px;
                }
                .error
                {
                    color:red;
                }
            </style>
            <title>Registration</title>
        </head>
        <body>
            <header>
            <h1>Register here</h1>
        </header>
        <form:form class="forcolor" action="registerProcess" method="post" modelAttribute="faculty" commandName="faculty">  
           <div class="regdetails">
                First name<br>
                <input class="field" path="firstName" type="text" name="firstName" id="firstName" required />
                <br>
                <form:errors path="firstName" cssClass="error" ></form:errors>
                <br>
                Last name<br>
                 <input class="field" type="text" name="lastName" required />
                 <br>
                <form:errors path="lastName" cssClass="error" ></form:errors>
                <br>
                Password<br>
                <input class="field" type="password" name="password" required />
                <br>
                <form:errors path="password" cssClass="error" ></form:errors>
                <br>
                E-mail/username<br>
                 <input class="field" type="email" name="email" required />
                 <br>
                <form:errors path="email" cssClass="error" ></form:errors>
                <br>
                Phone no<br>
                 <input class="field" type="number" name="phone" required />
                 <br>
                <form:errors path="phone" cssClass="error" ></form:errors>
                <br>
                Sex<br>
                <input type="radio" name="gender" value="male" required /> Male<br>
                <input type="radio" name="gender" value="female" required /> Female<br>
                <br>
                <input class="single" type="checkbox" checked="checked" /> Remember me<br>
                <input class="forsize" type="submit" value="SignUp" /><br>
                <div class="creating">
                    <p><a href="<c:url value="/login" />">already having an account? Login here</a></p>
                </div>
                <table>
                    <tr>
                            <td style="font-style: italic; color: red;">${message}</td>
                    </tr>
                </table>
            </div>
        </form:form> 
        </body>
    </html>

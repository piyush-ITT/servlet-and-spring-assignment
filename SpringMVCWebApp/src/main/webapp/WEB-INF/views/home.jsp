<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.List"%>
<html>
<head>
	<title>Home</title>
         <style type="text/css" rel="stylesheet">
                body
                {
                  background:#d2eff2;
                }
                 header
                {
                  text-align: center;
                  font-family: cursive;
                  color: blue;
                  margin-top: 20px;
                  font-weight: normal;
                  text-shadow: 2px 1px #2810A2;
                  font-size: 30px;
                }
                .creating a:hover
                {
                  color: red; 
                }
         </style>
</head>
<body>
    <header>
            <h1>Homepage</h1>
        </header>
    <table border="2" width="70%" cellpadding="2" align="center">  
        <caption>Faculty Information Table</caption>
            <tr><th>First Name</th><th>Last Name</th><th>Email</th><th>Password</th><th>Phone</th><th>Gender</th></tr>  
            <c:forEach var="faculty" items="${list}">   
                <tr>  
                    <td>${faculty.firstName}</td>  
                    <td>${faculty.lastName}</td>  
                    <td>${faculty.email}</td> 
                    <td>${faculty.password}</td> 
                    <td>${faculty.phone}</td> 
                    <td>${faculty.gender}</td>  
                </tr>  
            </c:forEach>  
    </table>
    <br>
    <div class="creating">
        <center><a href="<c:url value="/logout" />"><h2>Logout</h2></a></center>
    </div>
</body>
</html>

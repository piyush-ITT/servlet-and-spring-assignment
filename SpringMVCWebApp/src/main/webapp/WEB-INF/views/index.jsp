<%-- 
    Document   : index
    Created on : Aug 10, 2017, 3:49:55 PM
    Author     : piyush.tiwari
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Welcome</title>
         <style type="text/css" rel="stylesheet">
                body
                {
                  background:#d2eff2;
                }
                header
                {
                  text-align: center;
                  font-family: cursive;
                  color: blue;
                  margin-top: 20px;
                  font-weight: normal;
                  text-shadow: 2px 1px #2810A2;
                  font-size: 30px;
                }
                .creating a:hover
                {
                  color: red; 
                }
            </style>
    </head>
    <body>
        <header>
            <h1>Welcome</h1>
        </header>
        <div class="creating">
        <table align="center">
            <tr>
                <td><a href="login">Login</a>
                </td>
                <td><a href="register">Register</a>
                </td>
            </tr>
        </table>
        </div>
    </body>
    </html>

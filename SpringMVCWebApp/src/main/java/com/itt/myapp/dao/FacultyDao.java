/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.dao;

import com.itt.myapp.domain.Faculty;
import com.itt.myapp.domain.Login;
import java.util.List;

/**
 *
 * @author piyush.tiwari
 */
public interface FacultyDao 
{
  void register(Faculty faculty);
  Faculty validateUser(Login login);
  public List<Faculty> findAll();
  public Faculty registerValidation(Faculty faculty);
}

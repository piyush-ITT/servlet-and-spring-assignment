/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.dao;

import com.itt.myapp.controller.HomeController;
import com.itt.myapp.domain.Faculty;
import com.itt.myapp.domain.Login;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author piyush.tiwari
 */
@Repository
@Qualifier("facultyDao")
public class FacultyDaoImpl implements FacultyDao 
{
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
  @Autowired
  DataSource dataSource;
  
  @Autowired
  JdbcTemplate jdbcTemplate;
   /*
  Method for registering user into database 
  */
    @Override
    public void register(Faculty faculty) 
    {
        logger.info("inside the Dao process");
        String sql = "insert into facultyInformation values(?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, new Object[] {faculty.getFirstName(),
        faculty.getLastName(), faculty.getEmail(), faculty.getPassword(), faculty.getPhone(), faculty.getGender() });
        logger.info("-----Successfully data is inserted------");
    }
    
    /*
    Method for validation at login side
    */
    @Override
    public Faculty validateUser(Login login) 
    {
    String sql = "select * from facultyInformation where email='" + login.getUsername() + "' and password='" + login.getPassword()
    + "'";
    List<Faculty> faculty = jdbcTemplate.query(sql, new FacultyMapper());
    return faculty.size() > 0 ? faculty.get(0) : null;
    }
    
    /*
    Method to retrieve all data from database
    */
    @Override
    public List<Faculty> findAll() 
    {
        List<Faculty> faculties = jdbcTemplate.query("SELECT * FROM facultyInformation", new FacultyMapper());
        return faculties;
    }
    
    /*
    Method for validation at Register side
    */
    @Override
    public Faculty registerValidation(Faculty faculty) 
    {
        logger.info("inside register validation");
        if(faculty.getPassword().length() <=6 && faculty.getPhone().length() <= 9)
        {
            logger.info("length of password:" + faculty.getPassword().length());
            return null;
        }
        else
            return faculty;
    }
    
  }
   /*
    Custom RowMapper
    */
  class FacultyMapper implements RowMapper<Faculty> 
  {
  @Override
  public Faculty mapRow(ResultSet rs, int arg1) throws SQLException {
    Faculty faculty = new Faculty();
    faculty.setFirstName(rs.getString("first_name"));
    faculty.setLastName(rs.getString("last_name"));
    faculty.setPassword(rs.getString("password"));
    faculty.setEmail(rs.getString("email"));
    faculty.setPhone(rs.getString("phone"));
    faculty.setGender(rs.getString("gender"));
    return faculty;
  }
}

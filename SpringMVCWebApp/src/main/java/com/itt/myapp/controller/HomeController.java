package com.itt.myapp.controller;



import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
     * @param locale
     * @param model
     * @return 
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Locale locale, Model model) 
        {
		logger.info("Welcome to the ITT!");		
		model.addAttribute("itt", "This is sample message" );
		
		return "index";
	}
        
        /*
        Method to invalidate session after logout
        */      
        @RequestMapping(value = "/logout", method = RequestMethod.GET)
        public String logout(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes) 
        {
            logger.info("inside logout!");
           //ModelAndView mav = new ModelAndView("login");
           redirectAttributes.addFlashAttribute("logoutMessage", "Successfully logged out!!");
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null)
        {    
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        
        return "redirect:/login";//You can redirect wherever you want, but generally it's a good practice to show login screen again.
        }
        
            
	
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.controller;
import com.itt.myapp.domain.Faculty;
import com.itt.myapp.service.FacultyService;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author piyush.tiwari
 */
@RestController
public class RegistrationController 
{
  private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
  @Autowired
  public FacultyService facultyService;
  
  @RequestMapping(value = "/register", method = RequestMethod.GET)
  public ModelAndView showRegister(HttpServletRequest request, HttpServletResponse response) 
  {
    ModelAndView mav = new ModelAndView("register");
    mav.addObject("faculty", new Faculty());
    return mav;
  }
  /*
  Method checking for validation at server side and registering user;
  redirect to homepage if everything is correct
  */
  //@RequestMapping(value = "/registerProcess", method = RequestMethod.POST)
  @PutMapping(value = "registerProcess")
  public ModelAndView registerProcess(HttpServletRequest request, HttpServletResponse response, @Valid @ModelAttribute("faculty") Faculty faculty, BindingResult bindingResult ) 
  { 
                ModelAndView mav;
                logger.info("inside the registration process37");
                //Faculty faculty1 = facultyService.registerValidation(faculty);
                //null != faculty1
                
                if (!bindingResult.hasErrors())
                {
                    facultyService.register(faculty);
                    mav = new ModelAndView("home", "faculty", faculty.getFirstName());
                    List<Faculty> faculties = facultyService.findAll();
                    mav.addObject("list", faculties);
                    logger.info("inside the registration process460");
                    return mav;
                }
                else
                {
                    logger.info("inside the password null");
                    mav = new ModelAndView("register");
                    //mav.addObject("message", "Password/phone no. length must be greater than 6/9!!");
                    return mav;
                }
                              
  }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.controller;

import com.itt.myapp.domain.Faculty;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.itt.myapp.domain.Login;
import com.itt.myapp.service.FacultyService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class LoginController 
{
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	@Autowired
	FacultyService facultyService;

	//@RequestMapping(value = "/login", method = RequestMethod.GET)
	@GetMapping(value = "login")
	public ModelAndView showLogin(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("login");
		mav.addObject("login", new Login());
		return mav;
	}
        /*
        Method checking for authentication at login time
        */

    /**
     *
     * @param request
     * @param response
     * @param login
     * @param result
     * @return
     */
    
	//@RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
	@PostMapping(value = "loginProcess")
	public ModelAndView loginProcess(HttpServletRequest request, HttpServletResponse response,@ModelAttribute("login") Login login) 
        {
             logger.info("inside the login process");
		ModelAndView mav = null;
                
                Faculty faculty = facultyService.validateUser(login);
                if (null != faculty) 
                {
                    List<Faculty> faculties = facultyService.findAll();
                    
                    mav = new ModelAndView("home");
                    mav.addObject("first_name", faculty.getFirstName());
                    mav.addObject("list", faculties);
                } 
                else 
                {
                mav = new ModelAndView("login");
                mav.addObject("message", "Username or Password is wrong!!");
                }
                return mav;
                    
        }
        
        @ResponseBody
        @RequestMapping(value="/nextpage" ,method = RequestMethod.GET)
        public List <Faculty> getList()
        {
            return facultyService.findAll();
        }
}
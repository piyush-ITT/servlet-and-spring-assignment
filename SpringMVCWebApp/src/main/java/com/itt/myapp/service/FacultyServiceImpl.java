/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.service;

import com.itt.myapp.controller.HomeController;
import com.itt.myapp.dao.FacultyDao;
import com.itt.myapp.domain.Faculty;
import org.springframework.stereotype.Service;
import com.itt.myapp.domain.Login;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Service("facultyService")
public class FacultyServiceImpl implements FacultyService 
{
private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
    @Autowired
    FacultyDao facultyDao;
    
    @Override
    public void register(Faculty faculty) 
    {
        logger.info("inside the register process");
        facultyDao.register(faculty);
    }

    @Override
    public Faculty validateUser(Login login) 
    {
       return facultyDao.validateUser(login);
    }

    @Override
    public List<Faculty> findAll() 
    {
        return facultyDao.findAll();
    }

    @Override
    public Faculty registerValidation(Faculty faculty) 
    {
        return facultyDao.registerValidation(faculty);
    }

	

}

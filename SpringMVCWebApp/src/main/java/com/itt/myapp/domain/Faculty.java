/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.domain;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
//import org.hibernate.validator.constraints.Email;
//import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author piyush.tiwari
 */

public class Faculty 
{
    @Size(min=2, max=30, message = "First name must be greater than 1")
    private String firstName;
    @Size(min=2, max=30, message = "Last name must be greater than 1")
    private String lastName;
    @Size(min=8, max=30, message = "password must be greater than 7")
    private String password;
    @NotEmpty(message = "Email must not be empty") @Email
    private String email;
    @Size(min=10, max=20, message = "phone number must be greater than 9")
    private String phone;
    private String gender;
 
    public Faculty() {
 
    }
 
    public Faculty(String firstName, String lastName, String password, String email, String phone, String gender) 
    {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
    }
 
 
    public String getFirstName() {
        return firstName;
    }
 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
 
    public String getLastName() {
        return lastName;
    }
 
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


 
    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.itt.myapp.domain;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 *
 * @author piyush.tiwari
 */
@Aspect
@Component
public class DatabaseAccess 
{
   @Pointcut("execution(* com.service.FacultyServiceImplementation.*(..))")
    private void selectAll(){}
    @Before("selectAll()")
    public void beforeAdvice()
    {
            System.out.println("Acessing the Database Faculty");
    }
    @After("selectAll()")
    public void afterAdvice()
    {
            System.out.println("Faculty Data Secessfully Entered to the DataBase");
    }
    @AfterThrowing(pointcut="selectAll()",throwing="ex")
    public void afterThrowing(Exception ex)
    {
            System.out.println("Error Occured "+ex.getMessage());
    } 
}

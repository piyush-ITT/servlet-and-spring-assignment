package com.websystique.spring.configuration;

import java.util.Scanner;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;

import com.websystique.spring.domain.CsQuizMaster;
import com.websystique.spring.domain.HelloWorld;
import com.websystique.spring.domain.HelloWorldImpl;

@Configuration
public class HelloWorldConfig 
{
	private int a;
	private Scanner sc;

	@Bean(name="QuizTime")
	@Description("This is a sample Bean")
	public HelloWorld helloWorld() 
	{
		return QuizCategory();		
	}
	
	@Bean(name="QuizQuestion")
	@Description("This is a sample Bean")
	public HelloWorld helloWorld1() 
	{
		if(a==1)
			return new HelloWorldImpl();
		else
			return new CsQuizMaster();		
	}

	private HelloWorld QuizCategory() 
	{
		// TODO Auto-generated method stub
		System.out.println("From which category of quiz question you want to ask:");
		System.out.println("1. Math");
		System.out.println("2. CS");
		sc = new Scanner(System.in);
		a = sc.nextInt();
		if(a==1)
			return new HelloWorldImpl();
		else
			return new CsQuizMaster();
	}

}

package com.websystique.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.websystique.spring.configuration.HelloWorldConfig;
import com.websystique.spring.domain.HelloWorld;

public class AppMain {

	public static void main(String args[]) {
		AbstractApplicationContext context = new AnnotationConfigApplicationContext(HelloWorldConfig.class);
		HelloWorld bean = (HelloWorld) context.getBean("QuizTime");
		HelloWorld bean2 = (HelloWorld) context.getBean("QuizQuestion");
		bean.popQuestion();
		bean2.Questions();
		context.close();
		//QuizMasterService quizMasterService = new QuizMasterService();
        //quizMasterService.askQuestion();
        
		/*AbstractApplicationContext context = new AnnotationConfigApplicationContext(HelloWorldConfig.class);
        QuizMasterService obj = (QuizMasterService) context.getBean("CsQuizMaster");
        obj.askQuestion();*/

	}

}

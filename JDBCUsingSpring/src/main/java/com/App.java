package com;

import java.util.List;
import java.util.Scanner;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import com.config.AppConfig;
import com.domain.*;
import com.service.FacultyService;
public class App
{
	private static AbstractApplicationContext context;
	private static Scanner sc;
	public static void main(String[] args) 
	{
		int i;
		context = new AnnotationConfigApplicationContext(AppConfig.class);
		FacultyService facultyService = (FacultyService) context.getBean(FacultyService.class);
		sc = new Scanner(System.in);
		ForInformation forInformation = new ForInformation();
        do 
        {
        	//Menu which will show to user initially
	        System.out.println("Welcome enter the following operation you want to perform on faculty database:-");
	        System.out.println("Enter 0 for Exit");
	        System.out.println("1. Insert");
	        System.out.println("2. SelectAll");
	        System.out.println("3. Update");
	        System.out.println("4. Delete");
	        System.out.println("5. FindInfo");
	        Faculty faculty;
	        i = sc.nextInt();
	            switch(i)
	            {
		             case 1:
		            	 faculty = forInformation.InsertInformation();   // Method for Insert row
		            	 facultyService.addFaculty(faculty);
		                 break;
		             case 2:
		            	 List < Faculty > faculties = facultyService.findAll();
		     	         for (Faculty faculty1: faculties) 
		     	         {
		     	            System.out.println(faculty1);		// Method for Select data
		     	         }
		            	 break;
		             case 3:
		            	 System.out.println("Enter the email id of faculty whose information you want to update:");// Method for Update
		            	 String updateMe = sc.next();
		            	 faculty = forInformation.updateInformation();   // Method for Insert row
		            	 facultyService.editFaculty(faculty, updateMe);
		            	 break;
		             case 4:
		            	 System.out.println("Enter the email id of faculty whose information you want to delete:");	// Method for deleting table
		            	 String deleteMe = sc.next();
		            	 facultyService.deleteFaculty(deleteMe);
		            	 break;
		             case 5:
		            	 System.out.println("Enter the email id of faculty whose information you want to find:");
		            	 String findMe = sc.next();
		            	 faculty = facultyService.find(findMe);
		            	 faculty.print();
		            	 break;
		             default:
		                 System.out.println("-----------invalid input-----------\n");
	
	            }
	    }while (i != 0);
	
		 context.close();
	}

}
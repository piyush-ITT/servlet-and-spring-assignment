package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domain.Faculty;
import com.Dao.*;

@Service("facultyService")
public class FacultyServiceImplementation implements FacultyService 
{
	@Autowired
    FacultyDAO facultyDAO;

	public void addFaculty(Faculty faculty) 
	{
		facultyDAO.addFaculty(faculty);		
	}

	public void editFaculty(Faculty faculty, String email) 
	{
		facultyDAO.editFaculty(faculty, email);	
	}

	public void deleteFaculty(String email) 
	{
		facultyDAO.deleteFaculty(email);
	}

	public Faculty find(String email) 
	{
		return facultyDAO.find(email);
	}

	public List<Faculty> findAll() 
	{
		return facultyDAO.findAll();
	}

}

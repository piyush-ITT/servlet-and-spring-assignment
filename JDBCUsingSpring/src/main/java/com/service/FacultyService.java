package com.service;
import com.domain.Faculty;
import java.util.List;

public interface FacultyService 
{
	public void addFaculty(Faculty faculty);
	 
    public void editFaculty(Faculty faculty, String email);
 
    public void deleteFaculty(String email);
 
    public Faculty find(String email);
 
    public List < Faculty > findAll();
}

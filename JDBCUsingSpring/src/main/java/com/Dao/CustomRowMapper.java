package com.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.domain.Faculty;

public class CustomRowMapper implements RowMapper<Faculty>
{
		public Faculty mapRow(ResultSet rs, int rowNum) throws SQLException {
		Faculty faculty = new Faculty();
		faculty.setPassword(rs.getString("password"));
		faculty.setEmail(rs.getString("email"));
		return faculty;
	}
}

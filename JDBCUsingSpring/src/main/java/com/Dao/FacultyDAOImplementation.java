package com.Dao;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import com.domain.Faculty;

@Repository
@Qualifier("FacultyDAO")
public class FacultyDAOImplementation implements FacultyDAO
{
	@Autowired
    JdbcTemplate jdbcTemplate;
	
	@Autowired
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public void addFaculty(Faculty faculty) {
		// TODO Auto-generated method stub
		jdbcTemplate.update("INSERT INTO facultyInformation (first_name, last_name, password, email, phone, gender) VALUES (?, ?, ?, ?, ?, ?)",
	        faculty.getFirstName(), faculty.getLastName(), faculty.getPassword(), faculty.getEmail(), faculty.getPhone(), faculty.getGender());
	        System.out.println("Faculty Added!!");
	}

	public void editFaculty(Faculty faculty, String email) {
		// TODO Auto-generated method stub
		jdbcTemplate.update("UPDATE facultyInformation SET first_name = ? , last_name = ? , password = ? WHERE email = ? ",
	            faculty.getFirstName(), faculty.getLastName(), faculty.getPassword(), email);
	        System.out.println("Faculty Information Updated!!");
	}
	/*
	 * Deleting faculty from database using namedParameter
	 */
	public void deleteFaculty(String email) 
	{
		String SQL = "DELETE FROM facultyInformation WHERE email = :email";
		MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("email", email);
		System.out.println(email+"   "+namedParameters);
		namedParameterJdbcTemplate.update(SQL, namedParameters);
		//jdbcTemplate.update("DELETE from facultyInformation WHERE email = ? ", email);
        System.out.println("Faculty Information Deleted!!");
	}

	public Faculty find(String email) 
	{
		Faculty faculty = (Faculty)jdbcTemplate.queryForObject("SELECT email, password FROM facultyInformation where email = ? ", new Object[]{ email }, new CustomRowMapper());
	    return faculty;
	}

	public List<Faculty> findAll() 
	{
		List < Faculty > faculties = jdbcTemplate.query("SELECT * FROM facultyInformation", new BeanPropertyRowMapper<Faculty>(Faculty.class));
		return faculties;
	}

}

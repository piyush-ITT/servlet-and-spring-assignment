package com.domain;

public class Faculty 
{
	private String firstName;
    private String lastName;
    private String password;
    private String email;
    private String phone;
    private String gender;
 
    public Faculty() {
 
    }
 
    public Faculty(String firstName, String lastName, String password, String email, String phone, String gender) 
    {
        super();
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
    }
 
 
    public String getFirstName() {
        return firstName;
    }
 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
 
    public String getLastName() {
        return lastName;
    }
 
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


 
    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Override
    public String toString() 
	{
		System.out.println("----------------------------------------------------------------------------------------------------");
		System.out.println("First name  | Last name     | Password      |     Email         |       Phone        |  Gender      ");
		System.out.println("----------------------------------------------------------------------------------------------------");
        StringBuilder builder = new StringBuilder();
        builder.append(getFirstName() + "\t\t" + getLastName() + "\t\t" + getPassword() + "\t\t" +  
        				getEmail() + "\t\t" + getPhone() + "\t\t" + getGender());
        return builder.toString();
    }
	
	public void print()
	{
		System.out.println("------------------------------------");
		System.out.println(" Password      |     Email         |");
		System.out.println("------------------------------------");
		System.out.println("  " + getPassword() + "\t\t" +  getEmail() );
		
	}
}

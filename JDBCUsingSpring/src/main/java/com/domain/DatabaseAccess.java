package com.domain;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class DatabaseAccess 
{
	@Pointcut("execution(* com.service.FacultyServiceImplementation.*(..))")
	private void selectAll(){}
	@Before("selectAll()")
	public void beforeAdvice()
	{
		System.out.println("Acessing the Database Faculty");
	}
	@After("selectAll()")
	public void afterAdvice()
	{
		System.out.println("Faculty Data Secessfully Entered to the DataBase");
	}
	@AfterThrowing(pointcut="selectAll()",throwing="ex")
	public void afterThrowing(Exception ex)
	{
		System.out.println("Error Occured "+ex.getMessage());
	}
}